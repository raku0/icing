﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using ICING.Domain.Constans;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace ICING.Domain.Entities
{
    public class User : IdentityUser<Guid>
    {
        [RegularExpression("[a-zA-Z]+", ErrorMessage = "Only letters")]
        public string Name { get; set; }
        [RegularExpression("[a-zA-Z]+", ErrorMessage = "Only letters")]
        public string SurName { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreateDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime LastLogin { get; set; }
        public string City { get; set; }
        public bool IsAllowCookie { get; set; }
        public virtual ICollection<IdentityUserClaim<Guid>> Claims { get; set; }
        public virtual ICollection<IdentityUserLogin<Guid>> Logins { get; set; }
        public virtual ICollection<IdentityUserToken<Guid>> Tokens { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<ImageGallery> Images { get; set; }
        public virtual ICollection<Message> MessagesSend { get; set; }
        public virtual ICollection<Message> MessagesReceived { get; set; }
        public virtual ICollection<User> UserLikes { get; set; }
        public virtual ICollection<User> UserDisLikes { get; set; }
        public virtual ICollection<Pairs> Pairs { get; set; }

        public User()
        {
            CreateDate = DateTime.Now;
            IsAllowCookie = false;
            Images = new List<ImageGallery>();
            MessagesSend = new List<Message>();
            MessagesReceived = new List<Message>();
            UserLikes = new List<User>();
            UserDisLikes = new List<User>();
            Pairs = new List<Pairs>();
        }
        public static string GeneratePassword(int length, int numberOfNonAlphanumericCharacters)
        {
            char[] Punctuations = "!@#$%^&*()_-+=[{]};:>|./?".ToCharArray();
            if (length < 1 || length > 128)
            {
                throw new ArgumentException(nameof(length));
            }

            if (numberOfNonAlphanumericCharacters > length || numberOfNonAlphanumericCharacters < 0)
            {
                throw new ArgumentException(nameof(numberOfNonAlphanumericCharacters));
            }

            using var rng = RandomNumberGenerator.Create();
            var byteBuffer = new byte[length];

            rng.GetBytes(byteBuffer);

            var count = 0;
            var characterBuffer = new char[length];

            for (var iter = 0; iter < length; iter++)
            {
                var i = byteBuffer[iter] % 87;

                if (i < 10)
                {
                    characterBuffer[iter] = (char)('0' + i);
                }
                else if (i < 36)
                {
                    characterBuffer[iter] = (char)('A' + i - 10);
                }
                else if (i < 62)
                {
                    characterBuffer[iter] = (char)('a' + i - 36);
                }
                else
                {
                    characterBuffer[iter] = Punctuations[i - 62];
                    count++;
                }
            }

            if (count >= numberOfNonAlphanumericCharacters)
            {
                return new string(characterBuffer);
            }

            int j;
            var rand = new Random();

            for (j = 0; j < numberOfNonAlphanumericCharacters - count; j++)
            {
                int k;
                do
                {
                    k = rand.Next(0, length);
                }
                while (!char.IsLetterOrDigit(characterBuffer[k]));

                characterBuffer[k] = Punctuations[rand.Next(0, Punctuations.Length)];
            }

            return new string(characterBuffer);
        }
    }
}
