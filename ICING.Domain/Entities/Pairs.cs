﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ICING.Domain.Entities
{
    public class Pairs
    {
        [Key]
        public Guid Id { get; set; }
        public virtual User FirstUser { get; set; }
        public virtual User SecondUser { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime PairDate { get; set; }

        public Pairs()
        {
            PairDate = DateTime.Now;
            IsDeleted = false;
        }
    }
}
