﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Drawing;
using System.Text.Json.Serialization;

namespace ICING.Domain.Entities
{
    public class ImageGallery
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte[] Image { get; set; }
        [Required]
        public bool IsPrivate { get; set; }
        [Required]
        public bool IsAvatar { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }

        public bool ImageToByte(IFormFile file)
        {
            if (file != null)
            {
                if (file.ContentType.Contains("image"))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        file.CopyTo(ms);
                        Image = ms.GetBuffer();
                        ms.Dispose();
                    }
                    return true;
                }
            }
            return false;
        }
        public static bool IsValidImage(byte[] bytes)
        {
            try
            {
                using MemoryStream ms = new MemoryStream(bytes);
                System.Drawing.Image.FromStream(ms);
                ms.Dispose();
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }
        public static bool IsValidImage(string baseString)
        {
            try
            {
                IsValidImage(Convert.FromBase64String(baseString));
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }
    }
}
