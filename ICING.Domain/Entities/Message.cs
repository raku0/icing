﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace ICING.Domain.Entities
{
    public class Message
    {
        [Key]
        [JsonIgnore]
        public Guid Id { get; set; }
        public string Content { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public User Sender { get; set; }
        public User Receiver { get; set; }
        public byte[] Image { get; set; }

        public Message()
        {
            Date = DateTime.Now;
        }
    }
}
