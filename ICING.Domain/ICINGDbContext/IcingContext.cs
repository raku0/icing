﻿using ICING.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;


namespace ICING.Domain.ICINGDbContext
{
    public class IcingContext : IdentityDbContext<
        User, Role, Guid,
        IdentityUserClaim<Guid>, UserRole, IdentityUserLogin<Guid>,
        IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        public IcingContext(DbContextOptions<IcingContext> options) : base(options) {}
        public DbSet<ImageGallery> ImageGalleries { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Pairs> Pairs { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(b =>
            {
                b.HasMany(e => e.UserLikes);
                b.HasMany(e => e.UserDisLikes);
                // Each User can have many UserClaims
                b.HasMany(e => e.Claims)
                    .WithOne()
                    .HasForeignKey(uc => uc.UserId)
                    .IsRequired();

                // Each User can have many UserLogins
                b.HasMany(e => e.Logins)
                    .WithOne()
                    .HasForeignKey(ul => ul.UserId)
                    .IsRequired();

                // Each User can have many UserTokens
                b.HasMany(e => e.Tokens)
                    .WithOne()
                    .HasForeignKey(ut => ut.UserId)
                    .IsRequired();

                // Each User can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.User)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();

                b.HasMany(e => e.Images)
                .WithOne(ut => ut.User);

                b.HasMany(e => e.MessagesSend)
                .WithOne(ut => ut.Sender);

                b.HasMany(e => e.MessagesReceived)
                .WithOne(ut => ut.Receiver);
            });

            modelBuilder.Entity<Role>(b =>
            {
                // Each Role can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.Role)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();
            });
            modelBuilder.Entity<Pairs>(p =>
            {
                p.HasOne(u => u.FirstUser)
                .WithMany();
                p.HasOne(u => u.SecondUser)
                .WithMany();
            });
        }
    }
}
