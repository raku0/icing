﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ICING.Domain.Migrations
{
    public partial class UserGalleryUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ImageGallery_AspNetUsers_UserId",
                table: "ImageGallery");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ImageGallery",
                table: "ImageGallery");

            migrationBuilder.RenameTable(
                name: "ImageGallery",
                newName: "ImageGalleries");

            migrationBuilder.RenameIndex(
                name: "IX_ImageGallery_UserId",
                table: "ImageGalleries",
                newName: "IX_ImageGalleries_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ImageGalleries",
                table: "ImageGalleries",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ImageGalleries_AspNetUsers_UserId",
                table: "ImageGalleries",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ImageGalleries_AspNetUsers_UserId",
                table: "ImageGalleries");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ImageGalleries",
                table: "ImageGalleries");

            migrationBuilder.RenameTable(
                name: "ImageGalleries",
                newName: "ImageGallery");

            migrationBuilder.RenameIndex(
                name: "IX_ImageGalleries_UserId",
                table: "ImageGallery",
                newName: "IX_ImageGallery_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ImageGallery",
                table: "ImageGallery",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ImageGallery_AspNetUsers_UserId",
                table: "ImageGallery",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
