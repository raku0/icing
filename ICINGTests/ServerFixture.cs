﻿using ICING;
using ICING.Utils.RequestModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xunit;

namespace ICINGTests
{
    public class ServerFixture : IDisposable
    {
        public TestServer TestServer { get; private set; }
        public readonly HttpClient _client;
        public IConfiguration Configuration { get; private set; }
        public readonly string ServerLink = "https://localhost:44394/";
        public ServerFixture()
        {
            string directory = Directory.GetCurrentDirectory();
            Regex regex = new Regex(@"^(.*?)\\ICING");
            Match match = regex.Match(directory);
            if (match.Success)
            {
                directory = match.Value + "\\ICING";
            }
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
            TestServer = new TestServer(new WebHostBuilder()
                .UseEnvironment("Development")
                .UseContentRoot(directory)
                .UseConfiguration(new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("appsettings.json")                
                .Build()
            )
                .UseStartup<Startup>());
            _client = TestServer.CreateClient();
            AddHeader().Wait();
            
        }

        public void Dispose()
        {

        }        
        public async Task<bool> AddHeader()
        {
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetToken(
                Configuration["ApiTestAccountInDevelopment:Login"],
                Configuration["ApiTestAccountInDevelopment:Password"],
                ServerLink,
                _client));
            return true;
        }
        public async Task<string> GetToken(string apiLogin, string apiPassword,string serverLink, HttpClient client )
        {
            RequestLogin login = new RequestLogin
            {
                Login = apiLogin,
                Password = apiPassword
            };
            HttpResponseMessage tokenResponse = await client.PostAsync(serverLink + "api/account/login", ContentHelper.GetStringContent(login));
            tokenResponse.EnsureSuccessStatusCode();
            return await tokenResponse.Content.ReadAsStringAsync();
        }
    }
    [CollectionDefinition("ServerCollection")]
    public class ServerCollection : ICollectionFixture<ServerFixture>
    {

    }
    public static class ContentHelper
    {
        public static StringContent GetStringContent(object obj)
            => new StringContent(JsonConvert.SerializeObject(obj), Encoding.Default, "application/json");
    }
}
