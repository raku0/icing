﻿using ICING.Domain.Entities;
using ICING.Domain.ICINGDbContext;
using ICING.Utils.RequestModel;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xunit;

namespace ICINGTests
{
    [Collection("ServerCollection")]
    public class TestMessage
    {
        public IcingContext Context { get; set; }
        readonly ServerFixture Helper;
        public TestMessage(ServerFixture helper)
        {
            Helper = helper;
            Context = Helper.TestServer.Host.Services.GetService<IcingContext>();
        }
        [Fact]
        public async Task AddMessage()
        {
            User user = Context.Users.Take(1).FirstOrDefault();
            RequestMessage message = new RequestMessage
            {
                Content="Test message",
                Receiver=user.Id.ToString()
            };
            HttpResponseMessage messageResponse = await Helper._client.PostAsync(Helper.ServerLink + "api/account/message/addmessage", ContentHelper.GetStringContent(message));
            Assert.Equal(HttpStatusCode.OK, messageResponse.StatusCode);
        }
    }
}




