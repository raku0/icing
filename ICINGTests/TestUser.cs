using Xunit;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using ICING.Utils.RequestModel;
using ICING.Domain.ICINGDbContext;
using Microsoft.Extensions.DependencyInjection;

namespace ICINGTests
{
    [Collection("ServerCollection")]
    public class TestUser
    {
        private readonly string ServerLink = "https://localhost:44394/";
        public IcingContext Context { get; set; }
        readonly ServerFixture Helper;

        public TestUser(ServerFixture helper)
        {
            Helper = helper;
            Context = Helper.TestServer.Host.Services.GetService<IcingContext>();
        }
        [Fact]
        public async Task TestLogin()
        {
            RequestLogin login = new RequestLogin
            {
                Login = Helper.Configuration["ApiTestAccountInDevelopment:Login"],
                Password = Helper.Configuration["ApiTestAccountInDevelopment:Password"]
            };
            HttpResponseMessage tokenResponse = await Helper._client.PostAsync(ServerLink + "api/account/login", ContentHelper.GetStringContent(login));
            tokenResponse.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, tokenResponse.StatusCode);
            login.Password = "somerandom";
            tokenResponse = await Helper._client.PostAsync(ServerLink + "api/account/login", ContentHelper.GetStringContent(login));
            Assert.Equal(HttpStatusCode.Unauthorized, tokenResponse.StatusCode);
        }
        [Theory]
        [ClassData(typeof(UserTestData))]
        public async Task TestRegister(string login, string password, string email, string phoneNumber, HttpStatusCode statusCode)
        {
            HttpResponseMessage registerResponse = await Helper._client.PostAsync(ServerLink + "api/account/register", UserRegisterHelper.ConvertIntoRegister(login, password, email, phoneNumber));
            Assert.Equal(statusCode, registerResponse.StatusCode);
        }
    }
    public static class UserRegisterHelper
    {
        public static StringContent ConvertIntoRegister(string login, string password, string email, string phoneNumber)
        {
            return ContentHelper.GetStringContent(new RequestRegister() { Login = login, Password = password, Email = email, PhoneNumber = phoneNumber });
        }
    }
    public class UserTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            //Login Password Email PhoneNumber Nickname StatusCode
            yield return new object[] { "test", "", "testa@tes.com", "123456789", HttpStatusCode.BadRequest };
            yield return new object[] { "", "test", "test@tes.com", "345", HttpStatusCode.BadRequest };
            yield return new object[] { "test121", "test2345", "", "123456789", HttpStatusCode.BadRequest };
            yield return new object[] { "test12313", "test2345!", "test@tes.com", "123456789", HttpStatusCode.OK };
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
