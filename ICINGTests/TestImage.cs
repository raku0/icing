﻿using ICING.Domain.Entities;
using ICING.Domain.ICINGDbContext;
using Microsoft.Extensions.DependencyInjection;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xunit;

namespace ICINGTests
{
    [Collection("ServerCollection")]
    public class TestImage
    {
        public IcingContext Context { get; set; }
        readonly ServerFixture Helper;

        public TestImage(ServerFixture helper)
        {
            Helper = helper;
            Context = Helper.TestServer.Host.Services.GetService<IcingContext>();
        }
        [Fact]
        public async Task TestAddImageToGallery()
        {
            ImageGallery imageGallery;
            Bitmap imgMap = new Bitmap(100, 100, PixelFormat.Format24bppRgb);
            using (MemoryStream ms = new MemoryStream())
            {
                imgMap.Save(ms, ImageFormat.Jpeg);
                imageGallery = new ImageGallery
                {
                    Image = ms.GetBuffer(),
                    IsAvatar = false,
                    IsPrivate = false
                };
                ms.Dispose();
            }
            HttpResponseMessage imageResponse = await Helper._client.PostAsync(Helper.ServerLink + "api/account/image/AddImageToGallery", ContentHelper.GetStringContent(imageGallery));
            Assert.Equal(HttpStatusCode.OK, imageResponse.StatusCode);
        }
        [Fact]
        public async Task TestGetUserGallery()
        {
            HttpResponseMessage galleryResponse = await Helper._client.GetAsync(Helper.ServerLink + "api/account/image/GetUserGallery");
            Assert.NotEqual(HttpStatusCode.NotFound, galleryResponse.StatusCode);
        }
        [Fact]
        public async Task TestDeleteImage()
        {
            ImageGallery image = Context.ImageGalleries.Take(1).FirstOrDefault();
            HttpResponseMessage galleryDeleteResponse = await Helper._client.DeleteAsync(Helper.ServerLink + "api/account/image/deleteimage/"+image.Id);
            Assert.Equal(HttpStatusCode.OK, galleryDeleteResponse.StatusCode);
        }
    }
}
