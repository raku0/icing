﻿using ICING.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Net.Mail;
using System.Net;

namespace ICING.Infrastructure.Email
{
    public class EmailSender : IEmailSender
    {
        public IConfiguration Configuration { get; }
        readonly SmtpClient Client = new SmtpClient();
        readonly MailMessage Message = new MailMessage();
        readonly NetworkCredential NetworkCredential = new NetworkCredential();
        public EmailSender(IConfiguration configuration)
        {
            Configuration = configuration;
            NetworkCredential.UserName = Configuration["EmailAccount:Login"];
            NetworkCredential.Password = Configuration["EmailAccount:Password"];
            Client.Host = Configuration["EmailAccount:Host"];
            Client.EnableSsl = bool.Parse(Configuration["EmailAccount:UseSSL"]);
            Client.Port = int.Parse(Configuration["EmailAccount:Port"]);
        }
        public async Task<bool> SendEmail(string sendTo,string subject, string body)
        {
            Message.From = new MailAddress(Configuration["EmailAccount:Login"], Configuration["EmailAccount:Name"]);
            Message.To.Add(sendTo);
            Message.Subject = subject;
            Message.Body = body;
            Message.IsBodyHtml = true;
            await Client.SendMailAsync(Message);
            return true;
        }
    }
}
