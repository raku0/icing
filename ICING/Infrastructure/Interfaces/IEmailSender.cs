﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICING.Infrastructure.Interfaces
{
    interface IEmailSender
    {
        public Task<bool> SendEmail(string SendTo,string subject,string body);
    }
}
