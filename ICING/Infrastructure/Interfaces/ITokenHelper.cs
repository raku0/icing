﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICING.Infrastructure.Interfaces
{
    public interface ITokenHelper
    {
        string GenerateToken(string userLogin);
        string ReadUserLogin(HttpRequest request);
    }
}
