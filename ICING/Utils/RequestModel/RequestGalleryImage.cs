﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ICING.Utils.RequestModel
{
    public class RequestGalleryImage
    {
        [Required]
        public string Image { get; set; }
        [Required]
        public bool IsPrivate { get; set; }
        [Required]
        public bool IsAvatar { get; set; }
    }
}
