﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICING.Utils.RequestModel
{
    public class RequestMessage
    {
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public string Receiver { get; set; }
        public byte[] Image { get; set; }
    }
}
