﻿using ICING.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace ICING.Utils.JwtToken
{
    public class JwtTokenService : ITokenHelper
    {
        private readonly IConfiguration _config;
        public JwtTokenService(IConfiguration config)
        {
            _config = config;
        }
        public string GenerateToken(string userLogin)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:SecretKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, userLogin),
            };
            var token = new JwtSecurityToken(issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                claims: claims, expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        public string ReadUserLogin(HttpRequest request)
        {
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            string authHeader = request.Headers["Authorization"];
            authHeader = authHeader.Replace("Bearer ", "");
            //JwtSecurityToken tokenS = handler.ReadToken(authHeader) as JwtSecurityToken;
            JwtSecurityToken tokenS = handler.ReadJwtToken(authHeader);
            return tokenS.Claims.First(c => c.Type == "sub").Value;
        }
    }
}
