﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ICING.Domain.Entities;
using ICING.Domain.ICINGDbContext;
using ICING.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ICING.Controllers
{
    [Route("api/account/[controller]/[action]")]
    [ApiController]
    public class PairController : Controller
    {
        IcingContext Context { get; }
        ITokenHelper TokenHelper { get; }
        UserManager<User> UserManager { get; }
        public PairController(ITokenHelper tokenHelper, IcingContext context, UserManager<User> userManager)
        {
            TokenHelper = tokenHelper;
            Context = context;
            UserManager = userManager;
        }

        [HttpPost("{idUser}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> PostLike(string idUser)
        {
            bool isPair = false;
            User user = await UserManager.FindByNameAsync(TokenHelper.ReadUserLogin(Request));
            User userLiked = await UserManager.FindByIdAsync(idUser);
            user.UserLikes.Add(userLiked);
            if (userLiked.UserLikes.Contains(user))
            {
                Context.Pairs.Add(new Pairs { 
                    FirstUser=user,
                    SecondUser=userLiked
                });
                isPair = true;
            }
            return Ok(isPair);
        }
        [HttpPost("{idUser}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> PostDisLike(string idUser)
        {
            User user = await UserManager.FindByNameAsync(TokenHelper.ReadUserLogin(Request));
            user.UserDisLikes.Add(await UserManager.FindByIdAsync(idUser));
            return Ok();
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetPair()
        {
            User user = await UserManager.FindByNameAsync(TokenHelper.ReadUserLogin(Request));
            return new JsonResult(Context.Pairs.Where(u => u.FirstUser == user || u.SecondUser == user && u.IsDeleted == false));
        }
        [HttpDelete("{idUser}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DisPair(string idUser)
        {
            User user = await UserManager.FindByNameAsync(TokenHelper.ReadUserLogin(Request));
            User userPair = await UserManager.FindByIdAsync(idUser);
            Context.Pairs.Remove(Context.Pairs.Where(u => u.FirstUser == user && u.SecondUser == userPair || u.FirstUser == userPair && u.SecondUser == user).Single());
            Context.SaveChanges();
            return Ok();
        }
    }
}