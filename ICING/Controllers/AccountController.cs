﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ICING.Domain.Entities;
using ICING.Utils.RequestModel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ICING.Utils.JwtToken;
using ICING.Infrastructure.Interfaces;
using ICING.Domain.ICINGDbContext;
using Microsoft.EntityFrameworkCore;

namespace ICING.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        SignInManager<User> SignInManager { get; }
        UserManager<User> UserManager { get; }
        IcingContext Context { get; }
        ITokenHelper TokenHelper { get; }
        public AccountController(SignInManager<User> signInManager, UserManager<User> userManager,ITokenHelper tokenHelper,IcingContext context)
        {
            SignInManager = signInManager;
            UserManager = userManager;
            TokenHelper = tokenHelper;
            Context = context;
        }
        [HttpPost]
        public async Task<IActionResult> Login([FromBody]RequestLogin user)
        {
            IActionResult response = Unauthorized();
            var result = await SignInManager.PasswordSignInAsync(user.Login, user.Password, false, false);
            if (result.Succeeded)
            {
                response = Ok(TokenHelper.GenerateToken(user.Login));
                UserManager.FindByNameAsync(user.Login).Result.LastLogin = DateTime.Now;
            }
            return response;
        }
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RequestRegister user)
        {
            if (ModelState.IsValid)
            {
                if (await UserManager.FindByNameAsync(user.Login) == null)
                {
                    User userRegister = new User()
                    {
                        Email = user.Email,
                        EmailConfirmed = false,
                        PhoneNumber = user.PhoneNumber,
                        PhoneNumberConfirmed = false,
                        UserName = user.Login,
                    };
                    await UserManager.CreateAsync(userRegister, user.Password);
                    return Ok("Ok");
                }
                else
                {
                    return BadRequest("User busy.");
                }
            }
            return new JsonResult(ModelState.Values);
        }           
    }
}