﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ICING.Domain.Entities;
using ICING.Domain.ICINGDbContext;
using ICING.Infrastructure.Interfaces;
using ICING.Utils.RequestModel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace ICING.Controllers
{
    [Route("api/account/[controller]/[action]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        IcingContext Context { get; }
        ITokenHelper TokenHelper { get; }
        UserManager<User> UserManager { get; }

        public ImageController(ITokenHelper tokenHelper, IcingContext context, UserManager<User> userManager)
        {
            TokenHelper = tokenHelper;
            Context = context;
            UserManager = userManager;
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> AddImageToGallery([FromBody]RequestGalleryImage userImage)
        {
            if (ModelState.IsValid)
            {
                if (ImageGallery.IsValidImage(userImage.Image))
                {
                    User user = await UserManager.FindByNameAsync(TokenHelper.ReadUserLogin(Request));
                    ImageGallery imageGallery = new ImageGallery
                    {
                        Image = Convert.FromBase64String(userImage.Image),
                        IsAvatar = userImage.IsAvatar,
                        IsPrivate = userImage.IsPrivate,
                    };
                    user.Images.Add(imageGallery);
                    await UserManager.UpdateAsync(user);
                    return Ok("Success");
                }
                else
                    return BadRequest("Not valid image");
            }
            return new JsonResult(ModelState.Values);
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetUserGallery()
        {
            User user = await Context.Users.Include(us => us.Images).SingleAsync(us => us.UserName == TokenHelper.ReadUserLogin(Request));
            //IEnumerable<RequestGalleryImage> userImage = from b in user.Images select new RequestGalleryImage() { IsAvatar = b.IsAvatar, IsPrivate = b.IsPrivate, Image = b.Image.ToString() };
            return user.Images.Count == 0 ? (IActionResult)NotFound() : new JsonResult(user.Images);

        }
        [HttpDelete("{idImage}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteImage(string idImage)
        {
            User user = await Context.Users.Include(us => us.Images).SingleAsync(us => us.UserName == TokenHelper.ReadUserLogin(Request));
            bool result = user.Images.Remove(user.Images.Single(e => e.Id == Guid.Parse(idImage)));
            await UserManager.UpdateAsync(user);
            return result ? Ok("Success") : (IActionResult)BadRequest("Not valid id");
        }
    }
}