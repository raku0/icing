﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ICING.Domain.Entities;
using ICING.Domain.ICINGDbContext;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using ICING.Utils.RequestModel;
using ICING.Utils.JwtToken;
using ICING.Infrastructure.Interfaces;

namespace ICING.Controllers
{
    [Route("api/account/[controller]/[action]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IcingContext Context;
        UserManager<User> UserManager { get; }
        ITokenHelper TokenHelper { get; }

        public MessageController(IcingContext context, UserManager<User> userManager, ITokenHelper tokenHelper)
        {
            Context = context;
            UserManager = userManager;
            TokenHelper = tokenHelper;

        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> AddMessage([FromBody] RequestMessage message)
        {
            if (message.Content.Trim() == "" && message.Image.Length < 2 && ModelState.IsValid)
            {
                return BadRequest("Message cant be empty without image." + ModelState.Values);
            }
            User sender = await UserManager.FindByNameAsync(TokenHelper.ReadUserLogin(Request));
            User receiver = await UserManager.FindByIdAsync(message.Receiver);
            if (receiver == null)
            {
                return BadRequest("Problem find receiver.");
            }
            Message userMessage = new Message
            {
                Sender = sender,
                Receiver = receiver,
                Content = message.Content,
                Image = message.Image
            };
            sender.MessagesSend.Add(userMessage);
            await UserManager.UpdateAsync(sender);
            return Ok();
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetMessages()
        {
            User user = await Context.Users.Include(u => u.MessagesSend).Include(u => u.MessagesReceived).SingleAsync(us => us.UserName == TokenHelper.ReadUserLogin(Request));
            UserMessage userMessage =  new UserMessage() { MessagesSend = user.MessagesSend, MessagesReceived = user.MessagesReceived };
            return user.MessagesSend.Count > 0 || user.MessagesReceived.Count > 0 ? new JsonResult(userMessage) : (IActionResult)NotFound();
        }
    }
    class UserMessage
    {
        public ICollection<Message> MessagesSend { get; set; }
        public ICollection<Message> MessagesReceived { get; set; }
    }
}
