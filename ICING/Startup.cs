using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using ICING.Domain.ICINGDbContext;
using Microsoft.EntityFrameworkCore;
using ICING.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using System.IO;
using System.Text.RegularExpressions;
using ICING.Infrastructure.Interfaces;
using ICING.Utils.JwtToken;
using System.Drawing;
using System.Drawing.Imaging;

namespace ICING
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentity<User, Role>()
                .AddDefaultTokenProviders()
                .AddEntityFrameworkStores<IcingContext>();
            services.AddAuthorization();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:SecretKey"])),
                        ClockSkew = TimeSpan.FromMinutes(5)
                    };
                });
            services.AddTransient<ITokenHelper, JwtTokenService>();
            services.AddScoped<IcingContext>();           
            string directory = Directory.GetCurrentDirectory();
            Regex regex = new Regex(@"^(.*?)\\ICING");
            Match match = regex.Match(directory);
            //TOFIX
            //FOR MIGRATION DELETE IF ELSE AND LET ONLY ONE ADDDBCONTEXT
            if (match.Success)
            {
                directory = match.Value + "\\ICING";
            }
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Release")
            {
                services.AddDbContext<IcingContext>(options => options.UseSqlServer("Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=" +
                            directory + "\\App_Data\\IcingDb.mdf;" +
                            "Initial Catalog=IcingDb.mdf;Trusted_Connection=True;MultipleActiveResultSets=true"));
            }
            else
            {
                services.AddDbContext<IcingContext>(options => options.UseInMemoryDatabase("TestDb"));
            }

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Logowanie";
                options.LogoutPath = $"/Wyloguj";
            });
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            CreateRoles(serviceProvider).GetAwaiter().GetResult();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                PrepareDevelopmentServer(serviceProvider).GetAwaiter().GetResult();
                CreateAdmin(serviceProvider, true).GetAwaiter().GetResult();
            }
            else
                CreateAdmin(serviceProvider).GetAwaiter().GetResult();

            app.UseHttpsRedirection();
            app.UseStatusCodePages();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
        private async Task CreateRoles(IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<Role>>();
            string[] roleNames = { "Admin", "Moderator", "User", "Sponsor" };

            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    Role role = new Role { Name = roleName };
                    await RoleManager.CreateAsync(role);
                }
            }
        }
        private async Task CreateAdmin(IServiceProvider serviceProvider, bool isDevelop = false)
        {
            var UserManager = serviceProvider.GetRequiredService<UserManager<User>>();
            var adminExist = await UserManager.FindByNameAsync("Admin");
            User user;
            if (adminExist == null)
            {
                if (isDevelop)
                {
                    user = new User { UserName = Configuration["AdminAccountInDevelopment:Login"] };
                    await UserManager.CreateAsync(user, Configuration["AdminAccountInDevelopment:Password"]);
                }
                else
                {
                    user = new User
                    {
                        UserName = Configuration["AdminAccountRelease:Login"],
                        Email = Configuration["AdminAccountRelease:Email"],
                        EmailConfirmed = true,
                        PhoneNumber = Configuration["AdminAccountRelease:PhoneNumber"]
                    };
                    string generatedPassword = User.GeneratePassword(12, 2);
                    await UserManager.CreateAsync(user, generatedPassword);
                    //send email with password
                }
                await UserManager.AddToRoleAsync(user, "Admin");
            }
        }
        private async Task PrepareDevelopmentServer(IServiceProvider serviceProvider)
        {
            UserManager<User> UserManager = serviceProvider.GetRequiredService<UserManager<User>>();
            User user = new User { UserName = Configuration["ApiTestAccountInDevelopment:Login"] };
            await UserManager.CreateAsync(user, Configuration["ApiTestAccountInDevelopment:Password"]);
            await UserManager.AddToRoleAsync(user, "Admin");
            Bitmap imgMap = new Bitmap(100, 100, PixelFormat.Format24bppRgb);
            ImageGallery imageGallery;
            ImageGallery imageGallery1;
            using (MemoryStream ms = new MemoryStream())
            {
                imgMap.Save(ms, ImageFormat.Jpeg);
                imageGallery = new ImageGallery
                {
                    Image = ms.GetBuffer(),
                    IsAvatar = false,
                    IsPrivate = false,                   
                };
                imageGallery1 = new ImageGallery
                {
                    Image = ms.GetBuffer(),
                    IsAvatar = false,
                    IsPrivate = true,
                };
                ms.Dispose();
            }
            User userWithImage = await UserManager.FindByNameAsync(Configuration["ApiTestAccountInDevelopment:Login"]);
            userWithImage.Images.Add(imageGallery);
            userWithImage.Images.Add(imageGallery1);
            await UserManager.UpdateAsync(userWithImage);
            Message message = new Message
            {
                Content = "Test message",
                Receiver = user,
                Sender = user
            };
            Message message1 = new Message
            {
                Content = "Test message1",
                Receiver = user,
                Sender = user
            };
            user.MessagesSend.Add(message);
            user.MessagesSend.Add(message1);
            await UserManager.UpdateAsync(user);
        }
    }
}
